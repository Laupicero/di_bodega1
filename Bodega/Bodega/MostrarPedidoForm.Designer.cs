﻿namespace Bodega
{
    partial class MostrarPedidoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox
            // 
            this.groupBox.Size = new System.Drawing.Size(757, 244);
            // 
            // panel
            // 
            this.panel.Size = new System.Drawing.Size(757, 47);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(465, 326);
            this.button1.Size = new System.Drawing.Size(139, 32);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(631, 326);
            this.button2.Size = new System.Drawing.Size(139, 32);
            // 
            // labelTitle
            // 
            this.labelTitle.Size = new System.Drawing.Size(170, 26);
            this.labelTitle.Text = "Mostrar Pedidos";
            // 
            // MostrarPedidoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 385);
            this.Name = "MostrarPedidoForm";
            this.Text = "MostrarPedidoForm";
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
    }
}