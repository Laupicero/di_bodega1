﻿
namespace Bodega
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripPrincipal = new System.Windows.Forms.MenuStrip();
            this.salirToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tslCliente = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.tslRealizarPedido = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.tslMostrarPedidos = new System.Windows.Forms.ToolStripLabel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.panelIMG = new System.Windows.Forms.Panel();
            this.labelStatusStrip = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStripPrincipal.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripPrincipal
            // 
            this.menuStripPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem1,
            this.acercaDeToolStripMenuItem1});
            this.menuStripPrincipal.Location = new System.Drawing.Point(0, 0);
            this.menuStripPrincipal.Name = "menuStripPrincipal";
            this.menuStripPrincipal.Size = new System.Drawing.Size(826, 24);
            this.menuStripPrincipal.TabIndex = 0;
            this.menuStripPrincipal.Text = "menuStrip1";
            // 
            // salirToolStripMenuItem1
            // 
            this.salirToolStripMenuItem1.Name = "salirToolStripMenuItem1";
            this.salirToolStripMenuItem1.Size = new System.Drawing.Size(41, 20);
            this.salirToolStripMenuItem1.Text = "Salir";
            this.salirToolStripMenuItem1.Click += new System.EventHandler(this.salirToolStripMenuItem1_Click);
            this.salirToolStripMenuItem1.MouseLeave += new System.EventHandler(this.salirToolStripMenuItem1_MouseLeave);
            this.salirToolStripMenuItem1.MouseHover += new System.EventHandler(this.salirToolStripMenuItem1_MouseHover);
            // 
            // acercaDeToolStripMenuItem1
            // 
            this.acercaDeToolStripMenuItem1.Name = "acercaDeToolStripMenuItem1";
            this.acercaDeToolStripMenuItem1.Size = new System.Drawing.Size(80, 20);
            this.acercaDeToolStripMenuItem1.Text = "Acerca de...";
            this.acercaDeToolStripMenuItem1.Click += new System.EventHandler(this.acercaDeToolStripMenuItem1_Click);
            this.acercaDeToolStripMenuItem1.MouseLeave += new System.EventHandler(this.acercaDeToolStripMenuItem1_MouseLeave);
            this.acercaDeToolStripMenuItem1.MouseHover += new System.EventHandler(this.acercaDeToolStripMenuItem1_MouseHover);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.acercaDeToolStripMenuItem.Text = "Acerca de ...";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.tslCliente,
            this.toolStripButton2,
            this.tslRealizarPedido,
            this.toolStripButton3,
            this.tslMostrarPedidos});
            this.toolStrip1.Location = new System.Drawing.Point(0, 124);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(826, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::Bodega.Properties.Resources._581_5819580_user_client_client_images_png_clipart;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // tslCliente
            // 
            this.tslCliente.Name = "tslCliente";
            this.tslCliente.Size = new System.Drawing.Size(82, 22);
            this.tslCliente.Text = "Nuevo Cliente";
            this.tslCliente.Click += new System.EventHandler(this.tslCliente_Click);
            this.tslCliente.MouseLeave += new System.EventHandler(this.tslCliente_MouseLeave);
            this.tslCliente.MouseHover += new System.EventHandler(this.tslCliente_MouseHover);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::Bodega.Properties.Resources._62306_do_task_to_icons_list_item_computer;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            // 
            // tslRealizarPedido
            // 
            this.tslRealizarPedido.Name = "tslRealizarPedido";
            this.tslRealizarPedido.Size = new System.Drawing.Size(87, 22);
            this.tslRealizarPedido.Text = "Realizar Pedido";
            this.tslRealizarPedido.Click += new System.EventHandler(this.tslRealizarPedido_Click);
            this.tslRealizarPedido.MouseLeave += new System.EventHandler(this.tslRealizarPedido_MouseLeave);
            this.tslRealizarPedido.MouseHover += new System.EventHandler(this.tslRealizarPedido_MouseHover);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::Bodega.Properties.Resources._68_686834_vector_check_list_clipart_png_download_checklist_clipart;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "toolStripButton3";
            // 
            // tslMostrarPedidos
            // 
            this.tslMostrarPedidos.Name = "tslMostrarPedidos";
            this.tslMostrarPedidos.Size = new System.Drawing.Size(93, 22);
            this.tslMostrarPedidos.Text = "Mostrar pedidos";
            this.tslMostrarPedidos.Click += new System.EventHandler(this.tslMostrarPedidos_Click);
            this.tslMostrarPedidos.MouseLeave += new System.EventHandler(this.tslMostrarPedidos_MouseLeave);
            this.tslMostrarPedidos.MouseHover += new System.EventHandler(this.tslMostrarPedidos_MouseHover);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelStatusStrip});
            this.statusStrip.Location = new System.Drawing.Point(0, 550);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(826, 22);
            this.statusStrip.TabIndex = 3;
            this.statusStrip.Text = "statusStrip1";
            // 
            // panelIMG
            // 
            this.panelIMG.BackgroundImage = global::Bodega.Properties.Resources.bodega_termon;
            this.panelIMG.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelIMG.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelIMG.Location = new System.Drawing.Point(0, 24);
            this.panelIMG.Name = "panelIMG";
            this.panelIMG.Size = new System.Drawing.Size(826, 100);
            this.panelIMG.TabIndex = 1;
            // 
            // labelStatusStrip
            // 
            this.labelStatusStrip.Name = "labelStatusStrip";
            this.labelStatusStrip.Size = new System.Drawing.Size(148, 17);
            this.labelStatusStrip.Text = "Aplicación de Bodega.beta";
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 572);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panelIMG);
            this.Controls.Add(this.menuStripPrincipal);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStripPrincipal;
            this.Name = "FormPrincipal";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bodega";
            this.menuStripPrincipal.ResumeLayout(false);
            this.menuStripPrincipal.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripPrincipal;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
        private System.Windows.Forms.Panel panelIMG;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripLabel tslCliente;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripLabel tslRealizarPedido;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripLabel tslMostrarPedidos;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel labelStatusStrip;
    }
}

