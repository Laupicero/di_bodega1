﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bodega
{
    public partial class FormPrincipal : Form
    {
        public FormPrincipal()
        {
            InitializeComponent();
        }

        //Botón para salir del menú
        private void salirToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        
        //Botón para ver la ventana 'Acerca de...'
        private void acercaDeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AboutBoxBodega abb = new AboutBoxBodega();
            abb.Show();
        }

        
        //Para ver Clientes
        private void tslCliente_Click(object sender, EventArgs e)
        {
            NuevoClienteForm ncf = new NuevoClienteForm();
            ncf.MdiParent = this;
            ncf.WindowState = FormWindowState.Maximized
                ;
            ncf.Show();
        }

        //Para Realizar Pedidos
        private void tslRealizarPedido_Click(object sender, EventArgs e)
        {
            RealizarPedidoForm rpf = new RealizarPedidoForm();
            rpf.MdiParent = this;
            rpf.WindowState = FormWindowState.Maximized;
            rpf.Show();
        }

        //Para Mostrar Pedidos
        private void tslMostrarPedidos_Click(object sender, EventArgs e)
        {
            MostrarPedidoForm mpf = new MostrarPedidoForm();
            mpf.MdiParent = this;
            mpf.WindowState = FormWindowState.Maximized;
            mpf.Show();
        }


        //-------------------------------
        // Eventos Mouse Hover
        //-------------------------------
        private void tslCliente_MouseHover(object sender, EventArgs e)
        {
            labelStatusStrip.Text = "Crear Nuevo Cliente";
        }

        private void tslRealizarPedido_MouseHover(object sender, EventArgs e)
        {
            labelStatusStrip.Text = "Realizar Pedido";
        }

        private void tslMostrarPedidos_MouseHover(object sender, EventArgs e)
        {
            labelStatusStrip.Text = "Mostrar Todos los Pedidos";
        }

        private void acercaDeToolStripMenuItem1_MouseHover(object sender, EventArgs e)
        {
            labelStatusStrip.Text = "Información de la Aplicación";
        }

        private void salirToolStripMenuItem1_MouseHover(object sender, EventArgs e)
        {
            labelStatusStrip.Text = "Salir de la Aplicación";
        }


        //-------------------------------
        // Eventos Mouse Leave
        //-------------------------------
        private void tslCliente_MouseLeave(object sender, EventArgs e)
        {
            labelStatusStrip.Text = "Aplicación Bodega.beta";
        }

        private void tslRealizarPedido_MouseLeave(object sender, EventArgs e)
        {
            labelStatusStrip.Text = "Aplicación Bodega.beta";
        }

        private void tslMostrarPedidos_MouseLeave(object sender, EventArgs e)
        {
            labelStatusStrip.Text = "Aplicación Bodega.beta";
        }

        private void salirToolStripMenuItem1_MouseLeave(object sender, EventArgs e)
        {
            labelStatusStrip.Text = "Aplicación Bodega.beta";
        }

        private void acercaDeToolStripMenuItem1_MouseLeave(object sender, EventArgs e)
        {
            labelStatusStrip.Text = "Aplicación Bodega.beta";
        }
    }
}
